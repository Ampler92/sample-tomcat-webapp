package com.commerzbank.demo.business.control;

import com.commerzbank.demo.model.Greeting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ws.rs.core.MediaType;
import java.util.concurrent.atomic.AtomicLong;

@RestController
@CrossOrigin
public class GreetingController {

    private static final Logger LOG = LoggerFactory.getLogger(GreetingController.class);

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();
    private final InitialContext initialContext;

    public GreetingController() throws NamingException {
        initialContext = new InitialContext();
    }

    @RequestMapping("/greeting")
    public Greeting greeting(@RequestParam(value = "name", defaultValue = "World") String name) {
        LOG.info("[GreetingController][greeting] api called.");
        return new Greeting(counter.incrementAndGet(),
                String.format(template, name));
    }

    @RequestMapping(value = "/appTitle", produces = MediaType.TEXT_PLAIN)
    public String appTitle() {
        try {
            LOG.info("[GreetingController][appTitle] api called.");
            return (String) initialContext.lookup("java:comp/env/applicationTitle");
        } catch (NamingException e) {
            return "(could not find parameter applicationTitle in JNDI)";
        }
    }
}