import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MetanavigationComponent} from './meta-navigation.component';

describe('MetanavigationComponent', () => {
    let component: MetanavigationComponent;
    let fixture: ComponentFixture<MetanavigationComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [MetanavigationComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(MetanavigationComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should be created', () => {
        expect(component).toBeTruthy();
    });
});
