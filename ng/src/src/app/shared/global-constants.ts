/**
 * Global Varibales
 */
// Read application name and version from package file
declare function require(moduleName: string): any;

export const {version: appVersion, name: appName} = require('../../../package.json');

export const servicePath = '/api/';
export const restPostfix = 'rest/';
export const loggingPostfix = 'logging/';
export const loginPostfix = 'sec/userInfo';
export const logoutPostfix = 'sec/logout';
