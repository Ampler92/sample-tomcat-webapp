package com.commerzbank.demo.configuration.security;

import com.commerzbank.frame.security.spring.core.userdetails.krb5.KerberosGrantedAuthoritiesUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.core.io.FileSystemResource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.kerberos.authentication.KerberosAuthenticationProvider;
import org.springframework.security.kerberos.authentication.KerberosServiceAuthenticationProvider;
import org.springframework.security.kerberos.authentication.sun.GlobalSunJaasKerberosConfig;
import org.springframework.security.kerberos.authentication.sun.SunJaasKerberosClient;
import org.springframework.security.kerberos.authentication.sun.SunJaasKerberosTicketValidator;
import org.springframework.security.kerberos.web.authentication.SpnegoAuthenticationProcessingFilter;
import org.springframework.security.kerberos.web.authentication.SpnegoEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.validation.constraints.NotBlank;

/**
 * Kerberos authentication configuration.
 *
 * @author FRAME
 */
@Profile(value = {"not local"})
@Configuration
@EnableWebSecurity
@ConfigurationProperties(prefix = "kerberos")
@Order(Ordered.HIGHEST_PRECEDENCE)
public class KerberosSecurityConfiguration extends GlobalSecurityConfiguration {
    /**
     * The absolute path name of the Kerberos configuration file.
     */
    @NotBlank
    private String config;

    /**
     * The absolute path name of the Kerberos keytab file.
     */
    @NotBlank
    private String keytab;

    /**
     * The service principal name of the service to be authenticated against.
     */
    @NotBlank
    private String spn;

    /**
     * The used user details service augmented with the user's granted authorities and properties.
     */
    @Autowired
    private KerberosGrantedAuthoritiesUserDetailsService kerberosUserDetailsService;

    /**
     * SPNEGO authentication entry point dealing the negotiation with the browser.
     *
     * @return The SPNEGO entry point.
     */
    @Bean
    public SpnegoEntryPoint spnegoEntryPoint() {
        return new SpnegoEntryPoint("/");
    }

    /**
     * The filter processing the SPNEGO authentication header and creating a
     * Kerberos service request token out of it. With this token the authentication
     * manager is called for authentication.
     *
     * @param authenticationManager The authentication manager used in authentication.
     * @return The prepared SPNEGO authentication processing filter.
     */
    @Bean
    public SpnegoAuthenticationProcessingFilter spnegoAuthenticationProcessingFilter(
            AuthenticationManager authenticationManager) {
        SpnegoAuthenticationProcessingFilter filter = new SpnegoAuthenticationProcessingFilter();
        filter.setAuthenticationManager(authenticationManager);
        return filter;
    }

    /**
     * The globally provided JAAS configuration.
     *
     * @return The global JAAS configuration.
     */
    @Bean
    public GlobalSunJaasKerberosConfig globalSunJaasKerberosConfig() {
        GlobalSunJaasKerberosConfig config = new GlobalSunJaasKerberosConfig();
        config.setKrbConfLocation(this.config);
        return config;
    }

    /**
     * The authentication provider for Kerberos authentication utilizing Sun's JAAS
     * login module and custom user details service.
     *
     * @return The Kerberos authentication provider.
     */
    @Bean
    public KerberosAuthenticationProvider kerberosAuthenticationProvider() {
        KerberosAuthenticationProvider provider = new KerberosAuthenticationProvider();
        SunJaasKerberosClient client = new SunJaasKerberosClient();
        provider.setKerberosClient(client);
        provider.setUserDetailsService(kerberosUserDetailsService);
        return provider;
    }

    /**
     * The authentication provider which validates SPNEGO tokens. Therefore it uses
     * a Sun JRE based Kerberos ticket validator. Furthermore it requires a custom
     * user details service providing the the user's properties and granted
     * authorities, as Kerberos delivers solely a user name.
     *
     * @return The Kerberos service authentication provider.
     */
    @Bean
    public KerberosServiceAuthenticationProvider kerberosServiceAuthenticationProvider() {
        KerberosServiceAuthenticationProvider provider = new KerberosServiceAuthenticationProvider();
        provider.setTicketValidator(sunJaasKerberosTicketValidator());
        provider.setUserDetailsService(kerberosUserDetailsService);
        return provider;
    }

    /**
     * The SPNEGO/Kerberos ticket validator based upon Sun's JAAS login module.
     *
     * @return The Sun JAAS ticket validator.
     */
    @Bean
    public SunJaasKerberosTicketValidator sunJaasKerberosTicketValidator() {
        SunJaasKerberosTicketValidator validator = new SunJaasKerberosTicketValidator();
        validator.setServicePrincipal(this.spn);
        validator.setKeyTabLocation(new FileSystemResource(this.keytab));
        return validator;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(kerberosAuthenticationProvider())
                .authenticationProvider(kerberosServiceAuthenticationProvider());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.sessionManagement().enableSessionUrlRewriting(true)
                .and()
                .csrf().disable()
                .authorizeRequests()
                .accessDecisionManager(accessDecisionManager)
                .expressionHandler(expressionHandler)
                .antMatchers("/api/sec/**")
                .authenticated()
                .and()
                .exceptionHandling()
                .authenticationEntryPoint(spnegoEntryPoint())
                .and()
                .addFilterAt(spnegoAuthenticationProcessingFilter(authenticationManager()),
                        BasicAuthenticationFilter.class)
                .addFilterAfter(appTokenGeneratingFilter, SpnegoAuthenticationProcessingFilter.class);
    }

    public String getConfig() {
        return config;
    }

    public void setConfig(String config) {
        this.config = config;
    }

    public String getKeytab() {
        return keytab;
    }

    public void setKeytab(String keytab) {
        this.keytab = keytab;
    }

    public String getSpn() {
        return spn;
    }

    public void setSpn(String spn) {
        this.spn = spn;
    }
}
