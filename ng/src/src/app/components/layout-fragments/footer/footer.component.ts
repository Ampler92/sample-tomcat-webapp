import {Component, OnInit} from '@angular/core';

import {UserInfo} from '../../../models/userInfo.model';
import {AuthenticationService} from '../../../services/authentication.service';
import {ConfigurationService} from '../../../services/configuration.service';
import {appName, appVersion} from '../../../shared/global-constants';

@Component({
    selector: 'app-footer',
    templateUrl: './footer.component.html'
})
export class FooterComponent implements OnInit {
    userInfo: UserInfo;
    appVersion = appVersion;
    appName = appName;
    baseUrl: string;
    location: Location = window.location;
    today = Date.now();

    constructor(private authService: AuthenticationService,
                private configurationService: ConfigurationService) {
        this.baseUrl = this.configurationService.baseUrl();
    }

    ngOnInit() {
        this.authService.userInfoStore$.subscribe(userInfo => {
            this.userInfo = userInfo || {frmuser: 'unknown', frmtoken: '', permissions: []};
        });
    }
}
