import {Component} from '@angular/core';

import {environment} from '../../../environments/environment';
import {appVersion} from '../../shared/global-constants';

@Component({
    selector: 'app-main',
    templateUrl: './main.component.html',
})
export class MainComponent {
    appVersion = appVersion;
    env = environment;

    constructor() {
    }

}
