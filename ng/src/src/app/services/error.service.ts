import {LocationStrategy, PathLocationStrategy} from '@angular/common';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Injectable, Injector} from '@angular/core';
import {Observable} from 'rxjs/internal/Observable';
import {map} from 'rxjs/internal/operators/map';

import {appName, loggingPostfix} from '../shared/global-constants';
import {UtilityService} from '../utils/utilitys';
import {ConfigurationService} from './configuration.service';
import {UserService} from './user.service';

@Injectable()
export class ErrorService {
    serviceUrl: string;

    constructor(private injector: Injector,
                private http: HttpClient,
                private utilityService: UtilityService,
                private configurationService: ConfigurationService) {
        this.serviceUrl = this.configurationService.restUrl() + loggingPostfix;
    }

    log(error) {
        // Log error to console
        console.error('CustomError', error);
        // Send error to server
        const errorToSend = this.addContextInfo(error);
        return this.postError(errorToSend);

        // return loggingService.postLogs(errorToSend);
    }

    addContextInfo(error) {
        // Context details
        const name = error.name || null;
        const appId = appName;
        const userService = this.injector.get(UserService);
        const user = userService.getUserInfo() || null;
        const time = new Date().getTime();
        const id = appId + '-' + user.frmuser + '-' + time;
        const location = this.injector.get(LocationStrategy);
        const url = location instanceof PathLocationStrategy ? location.path() : '';
        const status = error.status || null;
        const message = error.message || error.toString();
        const stack = error instanceof HttpErrorResponse ? null : error.stack;

        const errorToSend = {name, appId, user, time, id, url, status, message, stack};
        return errorToSend;
    }

    postError(error: any): Observable<any> {
        console.log('Send error to server');
        return this.http.post<any>(this.serviceUrl, error, this.utilityService.buildHeader())
            .pipe(
                map(response => response)
            );
    }
}
