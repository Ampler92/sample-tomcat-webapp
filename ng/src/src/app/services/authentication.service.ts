import {Location} from '@angular/common';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {JwtHelperService} from '@auth0/angular-jwt';
import {Store} from '@ngrx/store';
import {NgxPermissionsService} from 'ngx-permissions';
import {Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';

import {UserInfo} from '../models/userInfo.model';
import {AppState} from '../ngrx/app-state';
import {ConfigurationService} from './configuration.service';
import {UserService} from './user.service';

@Injectable()
export class AuthenticationService {
    userInfoStore$: Observable<UserInfo>;
    private loginURL: string;
    private logoutURL: string;

    constructor(
        private location: Location,
        private http: HttpClient,
        private store: Store<AppState>,
        private userService: UserService,
        private configurationService: ConfigurationService,
        private router: Router,
        private permissionsService: NgxPermissionsService) {
        this.userInfoStore$ = this.store.select('userInfo');
        this.configurationService.setBaseUrl(this.deriveContext(location));
        this.loginURL = configurationService.loginUrl();
        this.logoutURL = configurationService.logoutUrl();
    }

    /**
     * Login user and save it to ngrx/store. Also save it to the session storage
     * so you don't have to authenticate again by page reloads
     */
    login(): Observable<boolean> {
        if (sessionStorage.getItem('userInfo')) {
            console.log(`Session already authenticated `);
            const userInfo: UserInfo = JSON.parse(sessionStorage.getItem('userInfo'));
            this.userService.saveUserInfo(userInfo);
            this.permissionsService.loadPermissions(userInfo.permissions);
            return of(true);
        } else {
            console.log(`Calling server with 'login' request from ${this.loginURL}`);
            return this.http.get(this.loginURL, {observe: 'response', responseType: 'text'}).pipe(
                map(res => {
                    const jwtHelper: JwtHelperService = new JwtHelperService();
                    const decodedToken = jwtHelper.decodeToken(res.headers.get('FRMTOKEN'));
                    console.log('FRMUSER: ', decodedToken.sub);
                    console.log('Permissions:', decodedToken.permissions);
                    const userInfo: UserInfo = {
                        frmuser: res.headers.get('FRMUSER'),
                        frmtoken: res.headers.get('FRMTOKEN'),
                        permissions: decodedToken.permissions.split(',')
                    };
                    // Write user to sessionStorage
                    sessionStorage.setItem('userInfo', JSON.stringify(userInfo));
                    this.userService.saveUserInfo(userInfo);
                    this.permissionsService.loadPermissions(userInfo.permissions);
                    return true;
                }),
                catchError((err: HttpErrorResponse) => {
                    console.error(err.message);
                    this.router.navigate(['errorpage/404']);
                    return of(false);
                })
            );
        }
    }

    /**
     * Remove user from ngrx/store sessionStorage and terminate session
     */
    logout() {
        this.userService.resetUserInfo();
        sessionStorage.removeItem('userInfo');
        this.terminateSession().subscribe(res => console.log('res: ' + res));
        this.router.navigate(['logout']);
    }

    /**
     * Terminate the session in backend
     */
    terminateSession() {
        return this.http.get(this.logoutURL, {responseType: 'text'});
    }

    /**
     * Check if the user is written in ngrx/store
     */
    isAuthenticated(): boolean {
        return true;
        // comment in to enable authentication. Currently disabled due to the lack of certificates:
        // return this.userService.getUserInfo() != null;
    }

    private deriveContext(location: Location): string {
        console.log('Location: ' + location.path());
        const context = window.location.href.replace(location.path(), '');
        console.log('Context: ' + context);
        return context;
    }
}
