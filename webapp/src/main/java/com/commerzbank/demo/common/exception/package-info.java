/**
 * Contains the application specific exceptions.
 */
package com.commerzbank.demo.common.exception;
