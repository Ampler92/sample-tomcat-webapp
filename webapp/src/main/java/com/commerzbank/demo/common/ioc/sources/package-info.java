/**
 * Contains the application specific property sources.
 */
package com.commerzbank.demo.common.ioc.sources;
