import {Component} from '@angular/core';
import {Router} from '@angular/router';

import {AuthenticationService} from '../../../services/authentication.service';

@Component({
    selector: 'app-meta-navigation',
    templateUrl: './meta-navigation.component.html'
})
export class MetanavigationComponent {
    navItems: String[];

    constructor(private authService: AuthenticationService,
                private router: Router) {
        this.navItems = ['Home', 'Contact', 'Help', 'Glossary', 'Language', 'Logout'];
    }

    click(id: any) {
        switch (id) {
            case 'Home': {
                this.router.navigate(['']);
                break;
            }
            case 'Logout': {
                this.authService.logout();
                break;
            }
        }
    }
}
