package com.commerzbank.demo.configuration.security;

import com.commerzbank.frame.security.spring.core.userdetails.memory.InMemoryGrantedAuthoritiesUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import java.util.ArrayList;
import java.util.List;

/**
 * Security configuration for local environments.
 *
 * @author FRAME
 */
@Profile(value = {"local"})
@Configuration
@EnableWebSecurity
@Order(Ordered.HIGHEST_PRECEDENCE)
public class BasicSecurityConfiguration extends GlobalSecurityConfiguration {
    /**
     * Authentication entry point dealing the BASIC authentication.
     *
     * @return The BASIC authentication entry point.
     */
    @Autowired
    private AppBasicAuthenticationEntryPoint authEntryPoint;

    /**
     * An in-memory user details service augmented with the user's granted authorities
     * and properties.
     *
     * @return The in-memory user details service to be used with the local profile.
     */
    @Bean
    public UserDetailsService inMemoryUserDetailsService() {
        List<UserDetails> users = new ArrayList<UserDetails>();
        users.add(User.withUsername("senior").password("senior").roles().build());
        users.add(User.withUsername("junior").password("junior").roles().build());
        return new InMemoryGrantedAuthoritiesUserDetailsService(users);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder builder) throws Exception {
        builder.userDetailsService(inMemoryUserDetailsService()).passwordEncoder(NoOpPasswordEncoder.getInstance());
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.sessionManagement().enableSessionUrlRewriting(true)
                .and()
                .headers().contentTypeOptions().disable()
                .and()
                .csrf().disable()
                .httpBasic()
                .authenticationEntryPoint(authEntryPoint)
                .and()
                .authorizeRequests()
                .accessDecisionManager(accessDecisionManager)
                .expressionHandler(expressionHandler)
                .antMatchers("/api/sec/**")
                .authenticated()
                .and()
                .addFilterAfter(appTokenGeneratingFilter, BasicAuthenticationFilter.class);
    }
}
