package com.commerzbank.demo.configuration;

import org.apache.catalina.startup.Tomcat;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.embedded.tomcat.TomcatWebServer;
import org.springframework.boot.web.servlet.server.ServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Configuration relevant if running the application on an embedded Tomcat server.
 *
 * @author FRAME
 */
@Configuration
public class EmbeddedServerConfiguration {

    @Bean
    public ServletWebServerFactory tomcatFactory() {
        return new TomcatServletWebServerFactory() {
            @Override
            protected TomcatWebServer getTomcatWebServer(Tomcat tomcat) {
                tomcat.enableNaming();
                return new TomcatWebServer(tomcat, getPort() >= 0);
            }
        };
    }
}
