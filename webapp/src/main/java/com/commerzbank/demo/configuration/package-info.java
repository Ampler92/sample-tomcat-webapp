/**
 * Contains the classes representing the configuration of the application.
 */
package com.commerzbank.demo.configuration;
