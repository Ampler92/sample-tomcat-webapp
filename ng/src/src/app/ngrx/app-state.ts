import {RouterState} from '@angular/router';

import {UserInfo} from '../models/userInfo.model';

export interface AppState {
    router: RouterState;
    userInfo: UserInfo;
}
