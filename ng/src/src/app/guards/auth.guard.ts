import {Injectable} from '@angular/core';
import {CanActivate} from '@angular/router';
import {Observable} from 'rxjs';

import {AuthenticationService} from '../services/authentication.service';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private authenticationService: AuthenticationService) {
    }

    canActivate(): boolean | Promise<boolean> | Observable<boolean> {
        if (this.authenticationService.isAuthenticated()) {
            return true;
        }
        return this.authenticationService.login();
    }
}
