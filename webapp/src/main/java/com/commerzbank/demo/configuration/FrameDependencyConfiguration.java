package com.commerzbank.demo.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * Loads the Spring XML default configurations of the FRAME components in use.
 *
 * @author FRAME
 */
@Configuration
@ImportResource("classpath:spring/frame-dependency-configuration.xml")
public class FrameDependencyConfiguration {
}
