import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs';

import {UserInfo} from '../models/userInfo.model';
import * as UserInfoActions from '../ngrx/actions/userInfo.actions';
import {AppState} from '../ngrx/app-state';

@Injectable()
export class UserService {
    userInfoStore$: Observable<UserInfo>;

    constructor(private store: Store<AppState>) {
        this.userInfoStore$ = this.store.select('userInfo');
    }

    saveUserInfo(userInfo: UserInfo) {
        this.store.dispatch(new UserInfoActions.Save(userInfo));
    }

    resetUserInfo() {
        this.store.dispatch(new UserInfoActions.Reset);
    }

    getUserInfo(): UserInfo {
        let userInfo: UserInfo = null;
        this.userInfoStore$.subscribe(user => userInfo = user);
        return userInfo;
    }
}
