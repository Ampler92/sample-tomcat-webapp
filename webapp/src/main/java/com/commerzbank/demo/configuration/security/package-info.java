/**
 * Contains the classes representing the security configuration of the application.
 */
package com.commerzbank.demo.configuration.security;
