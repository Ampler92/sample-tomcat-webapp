
var Agent = require("agentkeepalive");
var packageJson = require("./package.json")

// Proxy configuration for remote backend values.
// Adjust PROXY_CONFIG.target and PROXY_CONFIG.context to your needs.
// Use 'ng serve -proxy-config proxy.conf.js' to enable proxy feature.

const PROXY_CONFIG = [
    {
        // add your backend server url to the target property
        target: "http://localhost:8080/" + packageJson.name,
        context: "/api",
        secure: false,
        changeOrigin: true,

        loglevel: "debug",
        onError: onErrorFunction,
        onProxyRes: onProxyResponse,
        onProxyReq: onProxyRequest,
        agent: keepaliveAgent
    }
];

var pre = 'PROXY: ';

var keepaliveAgent = new Agent({
    maxSockets: 100,
    keepAlive: true,
    maxFreeSockets: 10,
    keepAliveMsecs: 1000,
    timeout: 60000,
    keepAliveTimeout: 30000
});

var onErrorFunction = function (err, req, res) {
    console.log(pre + 'An error occured');
}

var onProxyRequest = function (proxyReq, req, res) {
};

var onProxyResponse = function (proxyRes, req, res) {
    // always enable CORS
    proxyRes.headers['Access-Control-Allow-Origin'] = '*';

    var authType = proxyRes.headers['www-authenticate'];
    if (authType.startsWith('Negotiate')) {
        console.log(pre + 'Authentication scheme SPNEGO/Kerberos not supported. Only Basic Autentication is supported.');
        proxyRes.statusCode = 401;
        // Dump headers
        var keys = Object.keys(proxyRes.headers);
        keys.forEach(k => console.log(pre + 'Header: ' + k)
        )
        ;
    }
    ;
}

module.exports = PROXY_CONFIG;
