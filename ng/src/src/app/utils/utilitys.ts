import {HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Params, RouterStateSnapshot} from '@angular/router';
import {RouterStateSerializer} from '@ngrx/router-store';

import {UserInfo} from '../models/userInfo.model';
import {UserService} from '../services/user.service';

export interface RouterStateUrl {
    url: string;
    queryParams: Params;
}

/**
 * The RouterStateSerializer takes the current RouterStateSnapshot
 * and returns any pertinent information needed. The snapshot contains
 * all information about the state of the router at the given point in time.
 * The entire snapshot is complex and not always needed. In this case, you only
 * need the URL and query parameters from the snapshot in the store. Other items could be
 * returned such as route parameters and static route data.
 */
export class CustomRouterStateSerializer implements RouterStateSerializer<RouterStateUrl> {
    serialize(routerState: RouterStateSnapshot): RouterStateUrl {
        const {url} = routerState;
        const queryParams = routerState.root.queryParams;
        return {url, queryParams};
    }
}

/**
 * Used to build headers for http requests
 */
@Injectable()
export class UtilityService {
    constructor(private userService: UserService) {
    }

    /**
     * Returns frame JWT and frame user request options
     */
    buildHeader() {
        let myHeaders: HttpHeaders = new HttpHeaders;
        const userInfo: UserInfo = this.userService.getUserInfo();

        myHeaders = myHeaders.append('Content-Type', 'application/json');
        if (userInfo) {
            myHeaders = myHeaders.append('FRMJWT', userInfo.frmtoken);
            myHeaders = myHeaders.append('FRMUSER', userInfo.frmuser);
        } else {
            console.warn('No UserInfo');
        }
        return {headers: myHeaders};
    }
}
