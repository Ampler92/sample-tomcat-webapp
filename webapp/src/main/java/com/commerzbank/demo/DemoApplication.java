package com.commerzbank.demo;

import com.commerzbank.frame.common.ioc.context.support.FramePropertySourcesPlaceholderConfigurer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * The {@link SpringBootServletInitializer} is necessary, if the application has to be run as a WAR
 * on an external runtime. It is obsolete, if the application is started as a runnable JAR on an
 * embedded Tomcat server.
 *
 * @author FRAME
 */
@SpringBootApplication
@EnableJpaRepositories(
        basePackages = "com.commerzbank.demo.dataaccess.repository",
        repositoryFactoryBeanClass = com.commerzbank.frame.repository.CrudRepositoryFactoryBean.class)
public class DemoApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Bean
    public static FramePropertySourcesPlaceholderConfigurer
    framePropertySourcesPlaceholderConfigurer() {
        return new FramePropertySourcesPlaceholderConfigurer();
    }

    /**
     * Used by {@link SpringBootServletInitializer} and called only in case of a WAR running on an
     * external runtime.
     */
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(DemoApplication.class);
    }
}
