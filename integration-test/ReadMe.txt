For usage instructions, run "mvn site" and have a look at target/site/index.html

Notice that you have the EAR file created at least once, e.g. by running "mvn clean install" on the whole project.
