package com.commerzbank.demo.business.control;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller receiving errors from the UI for further processing on server side.
 *
 * @author FRAME
 */
@RestController
@RequestMapping(
        path = "/logging",
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE)
public class LoggingRestController {

    //@RequestMapping(path = "/", method = RequestMethod.POST)
    @PostMapping
    public @ResponseBody
    String logging(@RequestBody String myString) {
        // TODO: Do something with the frontend error...
        System.out.println(myString);
        return myString;
    }
}
