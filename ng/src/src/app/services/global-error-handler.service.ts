import {HttpErrorResponse} from '@angular/common/http';
import {ErrorHandler, Injectable, Injector} from '@angular/core';
import {Router} from '@angular/router';

import {ErrorService} from './error.service';

@Injectable()
export class GlobalErrorHandlerService implements ErrorHandler {

    constructor(private injector: Injector) {
    }

    handleError(error: any): void {
        const errorService = this.injector.get(ErrorService);
        const router = this.injector.get(Router);

        // TODO: Notification Service
        if (error instanceof HttpErrorResponse) {
            // Http error
            // Send error to backend
            errorService.log(error).subscribe();
        } else {
            // A client-side error occured
            errorService.log(error).subscribe(errorWithContextInfo => {
                router.navigate(['/error/1'], {queryParams: errorWithContextInfo});
            });
        }
    }
}
