import {Injectable} from '@angular/core';
import {loginPostfix, logoutPostfix, restPostfix, servicePath} from '../shared/global-constants';

@Injectable()
export class ConfigurationService {

    /* Right now, we have two ways to access the server: By property baseURL and via proxy
    configured in proxy.conf. You either call localhost:8080/web-0.0.1-SNAPSHOT/api/rest/
    or just api/rest. The latter is being translated into the former automatically by the proxy.*/

    baseURL = '';

    public setBaseUrl(url: string) {
        if (url.endsWith('/')) {
            url = url.slice(0, -1);
        }
        console.log('Using base URL: ' + url);
        this.baseURL = url;
    }

    // Application base URL
    public baseUrl(): string {
        return this.baseURL;
    }

    // login URL
    public loginUrl(): string {
        return "api/" + loginPostfix;
    }

    // logout URL
    public logoutUrl(): string {
        return "api/" + logoutPostfix;
    }

    // REST base URL
    public restUrl(): string {
        return this.serviceBase() + restPostfix;
    }

    protected serviceBase(): string {
        const storedUrl = localStorage.getItem('serviceUrl');
        let url: string;
        if (storedUrl) {
            url = storedUrl;
            console.log('WARNING: Changing Service Base URL based on browser local storage.');
        } else {
            url = this.baseURL;
        }
        return url + servicePath;
    }

}
