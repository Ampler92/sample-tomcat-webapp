package com.commerzbank.demo.pagemodel

import geb.Page;


class HomePage extends Page {
    static at = { $("h2").text() == "Anwendung auf Basis der FRAME-Architektur" }

    static content = {

    }
}