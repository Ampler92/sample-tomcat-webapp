import {UserInfo} from '../../models/userInfo.model';
import * as userInfo from '../../ngrx/actions/userInfo.actions';

const initialState: UserInfo = null;

const newState = (state, newData) => {
    return Object.assign({}, state, newData);
};

export function userInfoReducer(state: UserInfo = initialState, action: userInfo.Actions) {
    switch (action.type) {
        case userInfo.SAVE:
            return newState(state, action.payload);

        case userInfo.RESET:
            return initialState;

        default:
            return state;
    }
}
