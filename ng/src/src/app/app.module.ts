import {HttpClientModule} from '@angular/common/http';
import {ErrorHandler, NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {BrowserModule, Title} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {StoreModule} from '@ngrx/store';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {NgxPermissionsModule} from 'ngx-permissions';

import {environment} from '../environments/environment';
import {AppComponent} from './app.component';
import {AppRouting} from './app.routing';
import {ErrorPageComponent} from './components/error-page/error-page.component';
import {FooterComponent} from './components/layout-fragments/footer/footer.component';
import {MainNavigationComponent} from './components/layout-fragments/main-navigation/main-navigation.component';
import {MetanavigationComponent} from './components/layout-fragments/meta-navigation/meta-navigation.component';
import {MainComponent} from './components/main/main.component';
import {AuthGuard} from './guards/auth.guard';
import {userInfoReducer} from './ngrx/reducer/userInfo.reducer';
import {AuthenticationService} from './services/authentication.service';
import {UserService} from './services/user.service';
import {ConfigurationService} from './services/configuration.service';
import * as Utilitys from './utils/utilitys';
import {LogoutComponent} from './components/logout/logout.component';
import {GlobalErrorHandlerService} from './services/global-error-handler.service';
import {ErrorService} from './services/error.service';

@NgModule({
    declarations: [
        AppComponent,
        ErrorPageComponent,
        FooterComponent,
        MainComponent,
        MainNavigationComponent,
        MetanavigationComponent,
        LogoutComponent
    ],
    imports: [
        AppRouting,
        BrowserAnimationsModule,
        BrowserModule,
        FormsModule,
        HttpClientModule,
        StoreModule.forRoot({
            userInfo: userInfoReducer
        }),
        NgxPermissionsModule.forRoot(),
        !environment.production ? StoreDevtoolsModule.instrument() : []
    ],
    providers: [
        AuthenticationService,
        AuthGuard,
        ConfigurationService,
        UserService,
        Utilitys.UtilityService,
        GlobalErrorHandlerService,
        ErrorService,
        Title,
        {provide: ErrorHandler, useClass: GlobalErrorHandlerService}
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
