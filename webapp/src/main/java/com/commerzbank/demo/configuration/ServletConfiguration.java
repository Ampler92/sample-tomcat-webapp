package com.commerzbank.demo.configuration;

import com.commerzbank.frame.common.web.LogoutServlet;
import com.commerzbank.frame.common.web.UserInfoServlet;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tuckey.web.filters.urlrewrite.UrlRewriteFilter;

import java.util.Arrays;

/**
 * Servlet related configuration.
 *
 * @author FRAME
 */
@Configuration
public class ServletConfiguration {

    @Bean
    public ServletRegistrationBean<UserInfoServlet> addUserInfoMapping() {
        return new ServletRegistrationBean<>(new UserInfoServlet(), "/api/sec/userInfo");
    }

    @Bean
    public ServletRegistrationBean<LogoutServlet> addLogoutMapping() {
        return new ServletRegistrationBean<>(new LogoutServlet(), "/api/sec/logout");
    }

    @Bean
    public FilterRegistrationBean<UrlRewriteFilter> urlRewriteFilter() {
        UrlRewriteFilter urlRewriteFilter = new UrlRewriteFilter();
        FilterRegistrationBean<UrlRewriteFilter> regUrlRewriteFilter =
                new FilterRegistrationBean<>(urlRewriteFilter);
        regUrlRewriteFilter.addInitParameter("statusEnabled", "false");
        regUrlRewriteFilter.setName("urlRewriteFilter");
        regUrlRewriteFilter.setUrlPatterns(Arrays.asList("/*"));
        return regUrlRewriteFilter;
    }
}
