package com.commerzbank.demo;

/**
 * Key constants for {@code ApplicationResources.properties}.
 *
 * @author FRAME
 */
public class ApplicationResources {
    /**
     * Constant for the message resource saying that a record is saved.
     */
    public static final String MESSAGE_SAVED = "message.saved";

    /**
     * Constant for the message resource saying that a record was created.
     */
    public static final String MESSAGE_CREATED = "message.created";

    /**
     * Constant for the message resource saying that an action was canceled.
     */
    public static final String MESSAGE_CANCELED = "message.canceled";

    /**
     * Constant for the message resource saying that a record was deleted.
     */
    public static final String MESSAGE_DELETED = "message.deleted";

    /**
     * Constant for the message resource saying that a record was removed successfully.
     */
    public static final String MESSAGE_REMOVED = "message.removed";

    /**
     * Constant for the message resource saying that an entry was added
     */
    public static final String MESSAGE_ADDED = "message.added";
}
