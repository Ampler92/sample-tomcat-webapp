import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, ParamMap} from '@angular/router';

export class ErrorMessage {
    statuscode: number;
    message: string;
}

@Component({
    selector: 'app-error-page',
    templateUrl: './error-page.component.html'
})

export class ErrorPageComponent implements OnInit {
    errorMessage: ErrorMessage = new ErrorMessage();

    constructor(private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.route.paramMap.subscribe((paramMap: ParamMap) => this.setErrorMessage(+paramMap.get('id')));
    }

    setErrorMessage(status: number) {
        this.errorMessage.statuscode = status;

        switch (true) {
            case status >= 400 && status < 500: {
                this.errorMessage.message = 'Client Error';
                break;
            }
            case status >= 500 && status < 600: {
                this.errorMessage.message = 'Server Error';
                break;
            }
            case status === 1: {
                this.errorMessage.message = 'No permission';
                break;
            }
            case status === 2: {
                this.errorMessage.message = 'Not authenticated';
                break;
            }
            default: {
                this.errorMessage.message = 'Some other error';
                break;
            }
        }

    }
}
