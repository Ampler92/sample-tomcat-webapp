import {Component} from '@angular/core';
import {appName} from './shared/global-constants';
import {Title} from '@angular/platform-browser';
import {HttpClient} from "@angular/common/http";
import {Observable, Subscription} from "rxjs";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html'
})
export class AppComponent {
    appName = appName;
    appTitle: string;

    public constructor(
        private titleService: Title,
        private http: HttpClient
    ) {

        const $observable: Observable<string> = this.http.get("api/rest/appTitle", {responseType: 'text'}) as Observable<string>;
        const subscription: Subscription = $observable.subscribe(
            x => {
                this.appTitle = x;
                this.titleService.setTitle(this.appTitle);
            }
        );
    }
}
