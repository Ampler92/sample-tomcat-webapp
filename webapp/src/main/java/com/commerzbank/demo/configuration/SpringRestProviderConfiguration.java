package com.commerzbank.demo.configuration;

import com.commerzbank.frame.jws.rest.standards.CommerzbankStandardsHandlerInterceptor;
import com.commerzbank.frame.jws.rest.standards.ticket.ApplicationTicketProcessingAdapter;
import com.commerzbank.frame.jws.rest.standards.ticket.UserTicketProcessingAdapter;
import com.commerzbank.frame.jws.rest.standards.tracing.ActivityIdProcessingAdapter;
import com.commerzbank.frame.jws.rest.standards.tracing.MachineNameProcessingAdapter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Configuration of the Commerzbank communication standard features for Spring REST providers.
 *
 * @author FRAME
 */
@Configuration
public class SpringRestProviderConfiguration implements WebMvcConfigurer {

    @Bean
    public UserTicketProcessingAdapter userTicketProcessingAdapter() {
        return new UserTicketProcessingAdapter();
    }

    @Bean
    public ApplicationTicketProcessingAdapter applicationTicketProcessingAdapter() {
        return new ApplicationTicketProcessingAdapter();
    }

    @Bean
    public MachineNameProcessingAdapter machineNameProcessingAdapter() {
        return new MachineNameProcessingAdapter();
    }

    @Bean
    public ActivityIdProcessingAdapter activityIdProcessingAdapter() {
        return new ActivityIdProcessingAdapter();
    }

    @Bean
    public CommerzbankStandardsHandlerInterceptor commerzbankStandardsHandlerInterceptor() {
        return new CommerzbankStandardsHandlerInterceptor();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(commerzbankStandardsHandlerInterceptor());
    }
}
