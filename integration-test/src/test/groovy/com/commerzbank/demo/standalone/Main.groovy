package com.commerzbank.demo.standalone

import com.commerzbank.demo.StartPageGebIntegrationTest
import geb.Configuration
import org.junit.internal.TextListener
import org.junit.runner.JUnitCore
import org.junit.runner.Result
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import java.nio.file.Files
import java.nio.file.Paths
import java.util.regex.Matcher
import java.util.regex.Pattern

class Main {

    private static final Logger log = LoggerFactory.getLogger(Main.class)

    private static final String CHROME = "ChromeDriver32.exe"
    private static final String GECKO32 = "GeckoDriver32.exe"
    private static final String GECKO64 = "GeckoDriver64.exe"
    private static final String IEDRIVER32 = "IEDriver32.exe"
    private static final String IEDRIVER64 = "IEDriver64.exe"
    private static final String PHANTOMJS = "PhantomJS.exe"

    private static final String GEB_DRIVER_DEFAULT_LOCATION = "/target/dependency/"

    public static Configuration config = null

    static main(args) {

        switch (args.length) {
            case 1:
                readConfigFile(args[0], 'local')
                break
            case 2:
                readConfigFile(args[0], args[1])
                break
        }

        retrieveDriverFromJar(CHROME)
        retrieveDriverFromJar(GECKO32)
        retrieveDriverFromJar(GECKO64)
        retrieveDriverFromJar(IEDRIVER32)
        retrieveDriverFromJar(IEDRIVER64)
        retrieveDriverFromJar(PHANTOMJS)

        JUnitCore junit = new JUnitCore()
        junit.addListener(new TextListener(System.out))
        Result result = junit.run(StartPageGebIntegrationTest)
    }

    private static void readConfigFile(def source, def environment) {
        def slurper = new ConfigSlurper(environment)
        def configClass = slurper.classLoader.parseClass(new File(source))
        ConfigObject confObj = slurper.parse(configClass)

        config = new Configuration(confObj)
    }

    private static void retrieveDriverFromJar(String driver) {
        InputStream is = Main.class.getResourceAsStream('/' + driver)
        if (is == null) {
            log.warn("Driver " + driver + " was not found.")
            return
        }
        BufferedReader br = new BufferedReader(new InputStreamReader(is))
        Files.createDirectories(Paths.get(getFolderContainingJar() + GEB_DRIVER_DEFAULT_LOCATION))
        FileOutputStream fos = new FileOutputStream(new File(getFolderContainingJar(), GEB_DRIVER_DEFAULT_LOCATION + driver))
        byte[] buffer = new byte[1024]
        int bytesRead;

        while ((bytesRead = is.read(buffer)) != -1) {
            fos.write(buffer, 0, bytesRead)
        }
        fos.flush()
        br.close()
        is.close()
        fos.close()
    }

    private static String getFolderContainingJar() {
        String path = Main.class.getResource("")
        def beginIndex = path.indexOf('/') + 1
        Pattern p = Pattern.compile("/{1}[^/]+jar!")
        Matcher m = p.matcher(path)
        int endIndex = 0
        if (m.find()) {
            endIndex = m.start()
            return path.substring(beginIndex, endIndex)
        }
        return ""
    }
}
