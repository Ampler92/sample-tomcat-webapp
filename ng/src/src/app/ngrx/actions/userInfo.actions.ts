import {Action} from '@ngrx/store';

import {UserInfo} from '../../models/userInfo.model';

export const SAVE = '[UserInfo] Save User';
export const RESET = '[UserInfo] Reset User';

export class Save implements Action {
    readonly type = SAVE;

    constructor(public payload: UserInfo) {
    }
}

export class Reset implements Action {
    readonly type = RESET;
}

export type Actions = Save | Reset;
