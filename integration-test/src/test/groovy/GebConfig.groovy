import com.commerzbank.frame.ui.webtest.AuthMode
import com.commerzbank.frame.ui.webtest.factory.PhantomJSWebDriverFactory
import com.commerzbank.frame.ui.webtest.factory.WebTestConfiguration

environments {
    local {
        baseUrl = "http://localhost:8080/demo/"
        authMode = AuthMode.HTTP_BASIC_AUTH
        configuration = new WebTestConfiguration("senior", "senior", Locale.GERMAN)
        driver = { new PhantomJSWebDriverFactory(configuration).createWebDriver() }
    }

    test {
        baseUrl = "https://<hostname>:<port>/demo"
        authMode = AuthMode.NONE
        configuration = new WebTestConfiguration("username", "password", Locale.GERMAN)
        driver = { new PhantomJSWebDriverFactory(configuration).createWebDriver() }
    }
}

autoClearCookies = true
atCheckWaiting = true
waiting {
    timeout = 10
    retryInterval = 0.5
}
reportsDir = "target/geb-reports"