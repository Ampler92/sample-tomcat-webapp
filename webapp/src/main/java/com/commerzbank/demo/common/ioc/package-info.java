/**
 * Contains the application specific classes handling with Spring's IoC functionality.
 */
package com.commerzbank.demo.common.ioc;
