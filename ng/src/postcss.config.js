module.exports = {
    plugins: {
        "autoprefixer": {
            browsers: [
                'Chrome >= 45',
                'Firefox ESR',
                // NOT the Edge app version shown in Edge's "About" screen. See also https://github.com/Fyrd/caniuse/issues/1928
                'Edge >= 12',
                'Explorer >= 10',
                'iOS >= 9',
                'Safari >= 9',
                'Android >= 4.4',
                'Opera >= 30'
            ]
        },
        "postcss-clean": {}
    }
};
