import {RouterModule, Routes} from '@angular/router';

import {ErrorPageComponent} from './components/error-page/error-page.component';
import {MainComponent} from './components/main/main.component';
import {AuthGuard} from './guards/auth.guard';
import {LogoutComponent} from './components/logout/logout.component';

const appRoutes: Routes = [
    {path: 'errorpage/:id', component: ErrorPageComponent},
    {path: 'logout', component: LogoutComponent},
    {
        path: '', canActivate: [AuthGuard], children: [
            {path: 'demo1', component: MainComponent},
            {path: 'demo2', component: MainComponent},
        ]
    },
    {path: '**', redirectTo: '', pathMatch: 'full'}
];

export const AppRouting = RouterModule.forRoot(appRoutes);
