# CDP Sample Tomcat Web Application

The CDP Sample Tomcat Web Application is an example application demonstrating the usage of OpenShift in 
conjunction with CDP. It is intended to show both the deployment and the configuration management offered 
by CDP for customers using Tomcat and OpenShift. Furthermore, it may also serve as a template for new 
Tomcat applications to be created and onboarded.
The source code for this sample app is in Bitbucket repository - https://bitbucket.intranet.commerzbank.com:8443/projects/CDP/repos/cdp-examples/browse/sample-tomcat-webapp and it is open to every user in the bank.

This document describes how to set up the sample application from getting the sources to the actual 
deployment in OpenShift. It assumes that you already have read and write access to an OpenShift project.

## Project structure

Project `sample-tomcat-webapp` contains a web application and a module to manage openshift and CDP task:

* module `webapp` contains the backend (example Spring REST service)

* module `ng`contains the A`ng`gular frontend

* module `cicd` contains OpenShift and CDP management. Basically, you control different tasks (project 
  setup in OpenShift, project cleanup, CDP staging). 
  
* There may be further modules (e.g. `integration-test`) that will not be covered here.
  
## Clone the repository

    git clone ssh://git@bitbucket.intranet.commerzbank.com:7999/cdp/cdp-examples.git
    
or, if you want to use http:

    git clone https://bitbucket.intranet.commerzbank.com:8443/scm/cdp/cdp-examples.git
    
## Adjust pom.xml of sub project "cicd"

Open `pom.xml` in sub project `cicd` and set the parameter `os_project` to the name of your 
OpenShift project the application is to be deployed to:

    <!-- OpenShift project to use -->
    <os_project>cdp</os_project>

Also adjust the login url `os_url`to your target OpenShift installation: 

    <!-- Login URL to OpenShift console -->
    <os_url>https://console.ocp11.cloud.internal:8443</os_url>
    
Furthermore, you need to correctly set the OpenShift route `os_route`to your application:

    <!-- OpenShift route (hostname) to application -->
    <os_route>adpfrontend-devel.cloud.internal</os_route>
    
You can read more about frame-based applications in conjunction with OpenShift on the official
[frame documentation site](https://frame.intranet.commerzbank.com/developerviews/dv_p_os_builder.html), 
but please be aware that not every information there is totally up-to-date.

## Build the application

At first, make sure the application can be build:
```
cd sample-tomcat-webapp
mvn clean test
```
If you see build errors, make sure that you have the correct Maven toolchain settings. See Troubleshooting section at end
of this document.

##### Steps to generate token in the OpenShift

One can generate a temporary token which expires in 24 hours or can generate a permanent non expiring token.

#####Temporary token:
1. Login to the Openshift
2. Click the drop down people icon logo next to the logged in user in the top right corner of the Openshift console.
3. Select the first option ***Copy Login Command***.
4. This will copy the token in the clip board.

If no token are used during the build process, the default non expiring permanent oc_token within the POM.xml will be 
used. This behaviour can easily be altered by the user with the below command, as for example:-

```
mvn clean install -P staging -D oc_token=<oc_token>
``` 
#####Permanent token:

To generate permanent token, please perform the below steps:
1. In the Openshift console after login, select the appropriate project.
2. Click the ***Resources*** and then ***Secret*** from the left pane.
3. Next click any of the ***cicd-config-token*** token.
4. In the builder token, click the link ***Reveal Secret***.
5. Scroll to the bottom and copy the secret token by clicking the icon ***Copy to Clipboard***.

Next, create all required OpenShift configurations and start the build within OpenShift:
```
cd sample-tomcat-webapp/cicd
mvn post-integration-test -D oc_token=<oc_token> -P setup
```

If the maven run was successful, it will log the following output:

```
service "sample-tomcat-webapp" created
buildconfig "sample-tomcat-webapp-build" created
buildconfig "sample-tomcat-webapp-run" created
deploymentconfig "sample-tomcat-webapp" created
route "sample-tomcat-webapp" created
imagestream "sample-tomcat-webapp" created
```

You can watch now OpenShift building and deploying your application: Navigate to the 
OpenShift console URL, go to `Build` -> `Builds` and wait until both the build and run 
tasks are completed:

![Onboard CDP Sample Tomcat Web Application](screenshots/openshift_builds.png)

If it says `Assemble failed`, you will get some information about the cause by clicking on 
the application name and then on `View Logs`.

![View Logs](screenshots/build_logs.png)

Now, go to `Application` -> `Pods`. A minute after build, you should see the application 
displayed with status `Running`:

![Onboard CDP Sample Tomcat Web Application](screenshots/openshift_pods.png)

For deletion of all OpenShift configurations, deployments, pods and so on we have 
the profile `cleanup`. So if you want to completely start from scratch, you can execute

    mvn post-integration-test -D oc_token=<oc_token> -Pcleanup
 
but be informed -- once again -- that this will completely remove the application from 
the respective OpenShift cluster. 

## The Bitbucket Web Hook
The cdp-sample-tomcat-webapp comes with a predefined Bitbucket web hook automatically 
created by running Maven with the `setup` profile as described above. Try it out with: 

```
curl -X POST https://<openshift-url>/oapi/v1/namespaces/cdp/buildconfigs/sample-tomcat-webapp-build/webhooks/sample-tomcat-webapp-trigger-secret/generic
```
    
Upon a successful trigger, you should see the application building in the OpenShift UI again.

## Access the application

Open `https://<os_route>/sample-tomcat-webapp/`.

## Deployables, Configuration Items and the CDP Maven Plugin

A core concept of CDP are Deployables and Configuration Items.
 
### Configuration Items
 *Configuration Items* (*CI*s) 
strongly correspond to entities of the same name of XLDeploy. They define an atomic property 
which XLDeploy is able to set automatically by replacing a placeholder in the form 
of `{{propertyKey}}`. The CDP Maven Plugin (also known as *Delivery Generator*) reads their 
definitions from the `pom.xml` of the `cicd` sub project. We have several example 
configuration parameters included in there and now want to introduce one of them: The 
application title, declared as:

    <valueDescription>
        <name>applicationTitle</name>
        <tag>openShift</tag>
        <type>String</type>
        <value>SampleApplicationTitle</value>
        <scope>ENVIRONMENT</scope>
    </valueDescription>

(Unfortunately, they decided to denote a configuration item as "value description"). 
The corresponding placeholder for this item is present the configuration 
file `applicationConfig.yml` as below (slightly shortened):

    data:
      sample-tomcat-webapp.xml: >-
          <Context>
            <Environment name="applicationTitle" value="{{applicationTitle}}" />
          </Context>
          
### Deployables
A so-called *Deployable* typically consists of a configuration file holding one or more 
placeholders. As the application title is being read from file `applicationConfig.yml`, 
the deployable is designated in `pom.xml` (shortened again) as:

    <deployable>
        <file>${project.build.directory}/classes/applicationConfig.yml</file>
        <absoluteFile>${project.build.directory}/classes/applicationConfig.yml</absoluteFile>
        <name>applicationConfig-yml</name>
        <type>RESOURCESFILE</type>
    </deployable>

Like this, the CDP Maven Plugin knows which configuration files and parameters to extract 
in order to build its own archive (`delivery.zip`) which is will passed it further to the 
CDP pipeline (whose explanation would lead us off the rails now).

If the package generated by the plugin has been build and deployed correctly, it can be 
configured via the CDP UI (Internal name: SCMDB UI):

![SCMDB UI](screenshots/scmdbui.png)

#### The image deployable

This special type of deployable is a reference to the OpenShift image you want to deploy.
It contains all required coordinates to locate the image in your DEV environment.
```
<deployable>
    <id>5</id>
    <name>one-karten</name>
    <tag>one-karten</tag>
    <type>IMAGE</type>
    <repositoryURL>docker-registry-default.ocp03.cloud.internal</repositoryURL>
    <repositoryNamespace>onekarten-dev01</repositoryNamespace>
    <imageId>5bd44ab057b831c661f63a24a684e07d1c1ad0dea2687a1d30d6c3cc1f249911</imageId>
    <groupId>one-karten</groupId>
</deployable>
```

Explanation of the tags used:
* `<name>one-karten</name>` : Name of the Image Stream in your project in DEV environment to get the image from.

* `<repositoryURL>docker-registry-default.ocp03.cloud.internal</repositoryURL>` : Repository URL to get the image from.

* `<repositoryNamespace>onekarten-dev01</repositoryNamespace>` : Name of your project in DEV environment to get the image from. 

* `<imageId>5bd44ab057b831c661f63a24a684e07d1c1ad0dea2687a1d30d6c3cc1f249911</imageId>` : Id of the image to copy.  
  Get the value to use from `OpenShift->Builds->Images`, attribute 'Digest', without 'sha256:' prefix.

## Onboard the application to CDP

This is a pretty complicated task requiring special access which is explained on 
the [CDP Onboarding Confluence page](https://confluence.intranet.commerzbank.com/display/CDP/Onboarding+Projects).

## FAQ and best practices

### How to configure my server certificate

There are multiple ways to configure SSL with OpenShift. It is recommended to use the HA Proxy 
of the cloud cluster to handle the SSL communication. Therefore the route configuration needs to 
know your server certificate as well as the corresponding private key.
To use SCMDB and CDP for the staging of this configuration you need to take care how to configure
your template and put the values into SCMDB in the right way.

Declaring the configuration items in pom.xml:
```
<valueDescription>
    <name>app.server_certificate.pem</name>
    <tag>tag-${sampleTomcatWebAppModule}</tag>
    <type>String</type>
    <scope>ENVIRONMENT</scope>
</valueDescription>
<valueDescription>
    <name>app.server_certificate.key</name>
    <tag>tag-${sampleTomcatWebAppModule}</tag>
    <type>String</type>
    <confidential>true</confidential>
    <scope>ENVIRONMENT</scope>
</valueDescription>
```
Putting the begin and end string of the PEM format into your template and insert a placeholder for the part 
in the middle of the certificate or private key
```
apiVersion: v1
kind: Route
metadata:
  labels:
    app: sample-tomcat-webapp
  name: sample-tomcat-webapp
spec:
  host: {{openshift.route}}
  port:
    targetPort: tomcat-http
  tls:
    insecureEdgeTerminationPolicy: Redirect
    termination: edge
    certificate: |-
     -----BEGIN CERTIFICATE-----
     {{app.server_certificate.pem}}
     -----END CERTIFICATE-----
    key: |-
     -----BEGIN RSA PRIVATE KEY-----
     {{app.server_certificate.key}}
     -----END RSA PRIVATE KEY-----
  to:
    kind: Service
    name: sample-tomcat-webapp
  wildcardPolicy: None
```
Insert of the middle part as a one liner into SCM DB:
![value in SCMDB:](../screenshots/SCMDB-UI_Certificate.png)

Example for creating this one liner on a Linux box:
```
# converting a jks
keytool -importkeystore -srckeystore /users/cb3raas/tmp/cicd-test01.dev.apps.cloud.internal.jks    -destkeystore /users/cb3raas/tmp/cicd-test01.dev.apps.cloud.internal.p12    -srcstoretype jks    -deststoretype pkcs12
openssl pkcs12 -in /users/cb3raas/tmp/cicd-test01.dev.apps.cloud.internal.p12 -out /users/cb3raas/tmp/cicd-test01.dev.apps.cloud.internal.pem
# extracting the middle part of the cert 
openssl x509 -outform pem -in /users/cb3raas/tmp/cicd-test01.dev.apps.cloud.internal.pem | grep -v "\-\-\-" | awk 'NF {sub(/\r/, ""); printf "%s",$0;}' ; echo
# extracting the middle part of the private key
openssl rsa  -in /users/cb3raas/tmp/cicd-test01.dev.apps.cloud.internal.pem | grep -v "\-\-\-" | awk 'NF {sub(/\r/, ""); printf "%s",$0;}' ; echo
```

### Using persistent storage of openShift

If you would like to use persistent storage of openShift it is strongly recommended to ask Cloud Foundation for consulting.
The current setup of storage in openShift might lead to lost of data. 
Right now you are able to create a persistentVolumeClaim which will
provide storage for you. If this claim will be deleted all the data stored on that storage will
also be deleted.
That's why we recommend the contact to Cloud Foundation.

There is a way to prevent a persistentVolumeClaim of being deleted immediately.
But this is a workaround only. The configuration file in this example app is using this workaround.

 ![configuration file:](../screenshots/persistent_storage.png)

 ![delete persistent storage](../screenshots/delete_storage.png)


## Known issues and Troubleshooting

### No toolchain found error during Maven build

The Maven build uses a Maven toolchain definition for Node.js. Node.js is required in 
the `ng` module build. You have to add the following lines to your Maven `conf/toolchains.xml`:

```
<toolchain>
    <type>paths</type>
    <provides>
      <id>node-10</id>
    </provides>
    <configuration>
      <paths>
        <path>C:\apps\node\v10.4.1</path>
      </paths>
    </configuration>
</toolchain> 
```

Adjust the value in tag <path>...</paths> to your local Node.js installation.

### OpenShift cannot create or initialize the POD
*Note: this is a temporarily issue only. As soon as the FRAME Builder image is fixed, the issue
 will not occur anymore*.
 
Cloud Foundation started setting a Quota for newly created OpenShift projects.
You can check your quotas in OpenShift console at `Resources` -> `Quota`.

If quotas are set in your, you have to add a `resource` section to your build and deployment 
configurations.

Replace in all configuration the following line
```
resources: {}
```

with the following lines:
```
resources:
    limits:
      cpu: '1'
      memory: 1Gi
    requests:
      cpu: 200m
      memory: 512M
```

Note that you can set only cpu and memory values smaller or equal to your quota values.

### OpenShift Maven build fails with exit code 137

This is a UNIX error code describing an `Out of Memory` situation. Your container has 
not enough memory to execute e.g. the Maven build.
Increase the memory value in `resources -> limits`to at least 1GB.

### Missing cicd-provisioning user
For each OpenShift project, a private Docker registry is created. All the images you create go by default
to that private registry. For staging, CDP needs to copy images from your private repository (located in 
DEV stage) to any other stage. To do that, CDP needs the correct permission to access your private repository.
This issue is generally solved by the Cloud Operating Team that is adding a system account automatically to 
all new created projects. This account is named ```cicd-provisioning/cicd-pull```. You can check its existence
by selecting `Resources -> Service Accounts`.

If the user is not listed there, CDP will not be able to copy the image.

Projects created before Mai 2019 usually will not have the required user, because at that time the convention 
mentioned above (automatically adding the user to all new projects) was not in place.

### Changing route values

The host value of a route object is immutable. So if you change that value, which is basically the "value" of the 
route object, and the route object already exists in OpenShift, CDP will get an error when trying to update
the route object.

This is a known Bug. Currently, if you want to change the 'host' attribute of a route, the route object has to be
deleted manually before starting the deployment of the new route.

Since introduction of cdp-maven-plugin version 0.0.32 there is a new attribute available for RESOURCESFILE named 
updateMethod. This attribute could have the values patch, replace or apply. Default value is patch. In case
of a route configuration you can also use the updateMethod replace. This will lead to a destroy and recreate 
of the configuration which will allow you to modify the hostname of a route too.

 ![use updateMethod replace](../screenshots/use_updateMethod_replace.png)
 ![replace_route](../screenshots/replaceRoute.png)

