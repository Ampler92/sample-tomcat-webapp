package com.commerzbank.demo;

import com.commerzbank.frame.ui.webtest.FrameWebTestCase;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

/**
 * This class contains integration test cases.
 */
public class HomePageIntegrationTest extends FrameWebTestCase {

    /**
     * Runs Homepage test.
     */
    @Test
    public void testHomepage() {
        final WebDriver driver = getWebDriver();
        // driver....
    }
}
