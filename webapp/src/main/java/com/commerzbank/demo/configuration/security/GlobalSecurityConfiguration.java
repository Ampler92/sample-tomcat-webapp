package com.commerzbank.demo.configuration.security;

import com.commerzbank.frame.jws.rest.filter.AppTokenGeneratingFilter;
import com.commerzbank.frame.security.access.expression.FrameWebSecurityExpressionHandler;
import com.commerzbank.frame.security.authorization.ComAusAuthority;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.security.access.vote.AffirmativeBased;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * Global security configuration valid for all environments.
 *
 * @author FRAME
 */
public class GlobalSecurityConfiguration extends WebSecurityConfigurerAdapter {

    /**
     * Authority associated with the ComAus system in use for authorization.
     */
    @Autowired
    @Qualifier("security.authority")
    public ComAusAuthority authority;
    /**
     * The web security expression handler used during authorization.
     */
    @Autowired
    @Qualifier("security.frameWebSecurityExpressionHandler")
    public FrameWebSecurityExpressionHandler expressionHandler;
    /**
     * The used access decision manager used during authorization.
     */
    @Autowired
    @Qualifier("security.httpRequestAccessDecisionManager")
    public AffirmativeBased accessDecisionManager;
    /**
     * The filter implementation creating, storing and transporting the required authentication
     * headers to the using frontend.
     */
    @Autowired
    protected AppTokenGeneratingFilter appTokenGeneratingFilter;

    /**
     * The authentication manager used for authentication.
     *
     * @return The authentication manager in use.
     */
    @Bean(name = "authenticationManager")
    @Override
    protected AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }
}
