import {Router} from '@angular/router';
import {Component} from '@angular/core';

@Component({
    selector: 'app-main-navigation',
    templateUrl: './main-navigation.component.html'
})
export class MainNavigationComponent {
    constructor(private router: Router) {
    }

    routerLinkActive(routerLink: string) {
        return this.router.isActive(routerLink, false);
    }
}
